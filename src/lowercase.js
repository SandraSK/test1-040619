function lowercase(word) {
    if (typeof word !== 'string') {
        throw new Error('bad input is used');
    }
    return word.toLowerCase();
}
  
module.exports = lowercase;