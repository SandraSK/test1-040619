const lowercase = require('./lowercase');

describe('lowercase', () => {
    it('CAT => cat', () => {
        expect(lowercase('CAT')).toBe('cat');
    });
    it('error - bad input', () => {
        expect(() => {
            lowercase({ 'word': 'cat' });
        }).toThrow(/bad input/);
    });
});